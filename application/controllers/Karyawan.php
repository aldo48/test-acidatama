<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Karyawan extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->helper("url");
		$this->load->model('ModelKaryawan');
	}

	public function index()
	{
		$data = array(
			'data' => $this->ModelKaryawan->showAllData()->result()
		);

		$this->load->view('template/header');
		$this->load->view('view_karyawan', $data);
		$this->load->view('template/footer');
	}

	public function get_data()
	{

		$result = $this->ModelKaryawan->showAllData()->result();

		$output = array(
			'data' => $result,
		);
		echo json_encode($output);
	}

	public function tambah_karyawan()
	{
		$nip = $this->input->post('nip');
		$ket = $this->input->post('ket');
		$jenis_kelamin = $this->input->post('jenis_kelamin');
		$alamat = $this->input->post('alamat');
		$no_telp = $this->input->post('no_telp');
		$valid = null;

		if ($nip == null || "") {
			$msg = "Nomer Induk Pegawai belum di masukan !";
			$valid = false;
		} else if ($ket == null || "") {
			$msg = "Nama Karyawan belum di masukan !";
			$valid = false;
		} else if ($jenis_kelamin == null || "") {
			$msg = "Jenis kelamin belum di pilih !";
			$valid = false;
		} else if ($alamat == null || "") {
			$msg = "alamat belum diisi !";
			$valid = false;
		} else if ($no_telp == null || "") {
			$msg = "Nomor telepon belum di masukan !";
			$valid = false;
		} else {
			$msg = "Data Karyawan berhasil ditambahkan";
			$valid = true;
		}

		if ($valid == true) {
			$data = array(
				'nip' => $this->input->post('nip'),
				'ket' => $this->input->post('ket'),
				'jenis_kelamin' => $this->input->post('jenis_kelamin'),
				'alamat' => $this->input->post('alamat'),
				'no_telp' => $this->input->post('no_telp'),
				'status' => $this->input->post('status'),
			);
			$this->ModelKaryawan->save($data);
			echo json_encode(array(
				"status" => true,
				"msg" => $msg
			));
		} else {
			echo json_encode(array(
				"status" => false,
				"msg" => $msg
			));
		}
	}

	public function delete_karyawan()
	{
		$nip = $this->input->post('nip');
		$data = $this->ModelKaryawan->delete_karyawan($nip);
		echo json_encode($data);
	}

	public function detail_karyawan()
	{

		$nip = $this->input->post('nip');
		$data = $this->ModelKaryawan->detail($nip);

		echo json_encode($data);
	}

	public function update_karyawan()
	{
		$nip = $this->input->post('nip');
		$ket = $this->input->post('ket');
		$jenis_kelamin = $this->input->post('jenis_kelamin');
		$alamat = $this->input->post('alamat');
		$no_telp = $this->input->post('no_telp');
		$status = $this->input->post('status');
		$valid = null;

		if ($nip == null || "") {
			$msg = "Nomer Induk Pegawai tidak boleh kosong atau sama";
			$valid = false;
		} else if ($ket == null || "") {
			$msg = "Nama Karyawan harap diisi !";
			$valid = false;
		} else if ($jenis_kelamin == null || "") {
			$msg = "Jenis kelamin harus di pilih !";
			$valid = false;
		} else if ($alamat == null || "") {
			$msg = "alamat harap dilengkapi !";
			$valid = false;
		} else if ($no_telp == null || "") {
			$msg = "Nomor telepon harap diisi!";
			$valid = false;
		} else {
			$msg = "data Berhasi di perbaruhi";
			$valid = true;
		}

		if ($valid == true) {
			$data = array(
				'nip' => $this->input->post('nip'),
				'ket' => $this->input->post('ket'),
				'jenis_kelamin' => $this->input->post('jenis_kelamin'),
				'alamat' => $this->input->post('alamat'),
				'no_telp' => $this->input->post('no_telp'),
				'status' => $this->input->post('status'),
			);
			$this->ModelKaryawan->update(array('nip' => $this->input->post('nip')), $data);
			echo json_encode(array(
				"status" => true,
				"msg" => $msg
			));
		} else {
			echo json_encode(array(
				"status" => false,
				"msg" => $msg
			));
		}
	}



	public function print_all()
	{
		$data = array('data' => $this->ModelKaryawan->showAllData()->result_array());
		// $html = $this->load->view('V_produksi_a200a_001', $data, true);
		$html = $this->load->view('cetak', $data, true);
		$mpdf = new \Mpdf\Mpdf([
			'orientation' => 'P',
			'format'      => array(210, 330)
		]);

		ini_set("pcre.backtrack_limit", "5000000");
		$mpdf->setFooter('{PAGENO} of {nb}');
		$mpdf->useSubstitutions = false;
		$mpdf->simpleTables = true;
		$mpdf->WriteHTML($html);
		$mpdf->Output();
	}
}
