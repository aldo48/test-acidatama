<?php defined('BASEPATH') or exit('No direct script access allowed');

class ModelKaryawan extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    function showAllData()
    {
        return $this->db->query("SELECT * FROM karyawan");
    }

    function detail($id)
    {
        $query = $this->db->query("SELECT * from karyawan where nip='$id'");
        return $query->row();
    }

    public function update($where, $data)
    {
        $this->db->update('karyawan', $data, $where);
        return $this->db->affected_rows();
    }

	public function delete_karyawan($id)
    {
        $this->db->where('nip', $id);
        return $this->db->delete('karyawan');
    }

    function save($data)
    {
        $this->db->insert('karyawan', $data);
        return $this->db->insert_id();
    }
}
