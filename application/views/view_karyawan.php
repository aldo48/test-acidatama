<div class="container-fluid">
    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">

        <button onclick="tambah_karyawan()" class="d-none d-sm-inline-block btn btn-sm btn-success shadow-sm"> Tambah Karyawan</button>
        <a href="<?php echo base_url('karyawan/print_all') ?>" target="_blank" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="fas fa-download fa-sm text-white-50"></i> Cetak Data Karyawan</a>
    </div>

    <div class="card shadow mb-4">
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>NIP</th>
                            <th>Nama</th>
                            <th>Nomer Telp</th>
                            <th>
                                <center>Status</center>
                            </th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tfoot>

                    </tfoot>
                    <tbody>
                        <?php
                        $i = 1;
                        foreach ($data as $row) {
                            echo "<tr>";
                            echo "<td>" . $i . "</td>";
                            echo "<td>" . $row->nip . "</td>";
                            echo "<td>" . $row->ket . "</td>";
                            echo "<td>" . $row->no_telp . "</td>";
                            echo "<td>" . (($row->status == "true") ? '<center><button class="btn btn-sm btn-success " disabled>AKTIF</button></center>' : '<center><button class="btn btn-sm bg-gray-500" disabled>TIDAK AKTIF</button></center>') . "</td>";
                            echo '<td>
                                <a class="btn btn-sm btn-success" href="javascript:void(0)" title="View" onclick="detail_karyawan(' . "'" . $row->nip . "'" . ')"><i class="glyphicon glyphicon-eye-open"></i> Detail</a>
                                <a class="btn btn-sm btn-primary" href="javascript:void(0)" title="Edit" onclick="edit_karyawan(' . "'" . $row->nip . "'" . ')"><i class="glyphicon glyphicon-pencil"></i> Edit</a>
                                <a class="btn btn-sm btn-danger" href="javascript:void(0)" title="Hapus" onclick="delete_karyawan(' . "'" . $row->nip . "'" . ')"><i class="glyphicon glyphicon-trash"></i> Delete</a>
                            </td>';
                            echo "</tr>";
                            $i++;
                        }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>


    <!-- Bootstrap modal insert-->
    <div class="modal fade" id="modal_form" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document" style=" width: 100%;">
            <div class="modal-content" style="height: 50%;  min-height: 100%;  border-radius: 0;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body form">
                    <form action="#" id="form" class="form-horizontal">
                        <div class="form-body">
                            <input type="hidden" value="" name="nip" />
                            <div class="form-group">
                                <label class="control-label col-md-8">Nomor Induk Karyawan</label>
                                <div class="col-md-12">
                                    <input name="nip" id="nip" placeholder="Nip" class="form-control" type="number">
                                    <span id="cek_nip"></span>
                                    <!-- <span class="help-block"></span> -->
                                </div>
                            </div>
                            <div class="form-group ">
                                <label class="control-label col-md-8">Nama Karyawan</label>
                                <div class="col-md-12">
                                    <input name="ket" id="ket" placeholder="Nama Karyawan" class="form-control" type="text">
                                    <span class="help-block"></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-8">Jenis Kelamin</label>
                                <div class="col-md-9">
                                    <select class="form-control" name="jenis_kelamin" id="jenis_kelamin">
                                        <option value="">--Pilih Jenis Kelamin--</option>
                                        <option value="L">Laki-Laki</option>
                                        <option value="P">Perempuan</option>
                                    </select>
                                    <span class="help-block"></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-8">Alamat</label>
                                <div class="col-md-12">
                                    <textarea name="alamat" id="alamat" placeholder="Alamat" class="form-control" type="text"></textarea>
                                    <span class="help-block"></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-8">Nomor Telpon</label>
                                <div class="col-md-12">
                                    <input name="no_telp" id="no_telp" placeholder="Nomor Telepon" class="form-control" type="text">
                                    <span class="help-block"></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-8">Status</label>
                                <div class="col-md-9">
                                    <select class="form-control" name="status" id="status">
                                        <option value="true">Aktif</option>
                                        <option value="false">Tidak Aktif</option>
                                    </select>
                                    <span class="help-block"></span>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" id="btnSave" onclick="save()" class="btn btn-primary">Simpan</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

    <!-- Bootstrap modal Detail-->
    <div class="modal fade" id="modal_detail" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document" style=" width: 100%;">
            <div class="modal-content" style="height: 50%;  min-height: 100%;  border-radius: 0;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body form">
                    <form action="#" id="form" class="form-horizontal">
                        <div class="form-body">
                            <input disabled type="hidden" value="" name="nip" />
                            <div class="form-group">
                                <label class="control-label col-md-8">Nomor Induk Karyawan</label>
                                <div class="col-md-12">
                                    <input disabled name="nip" id="nip_detail" placeholder="Nip" class="form-control" type="number">
                                    <span id="cek_nip"></span>
                                    <!-- <span class="help-block"></span> -->
                                </div>
                            </div>
                            <div class="form-group ">
                                <label class="control-label col-md-8">Nama Karyawan</label>
                                <div class="col-md-12">
                                    <input disabled name="ket" id="ket_detail" placeholder="Nama Karyawan" class="form-control" type="text">
                                    <span class="help-block"></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-8">Jenis Kelamin</label>
                                <div class="col-md-9">
                                    <select disabled class="form-control" name="jenis_kelamin" id="jenis_kelamin_detail">
                                        <option value="">--Pilih Jenis Kelamin--</option>
                                        <option value="L">Laki-Laki</option>
                                        <option value="P">Perempuan</option>
                                    </select>
                                    <span class="help-block"></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-8">Alamat</label>
                                <div class="col-md-12">
                                    <textarea disabled name="alamat" id="alamat_detail" placeholder="Alamat" class="form-control" type="text"></textarea>
                                    <span class="help-block"></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-8">Nomor Telpon</label>
                                <div class="col-md-12">
                                    <input disabled name="no_telp" id="no_telp_detail" placeholder="Nomor Telepon" class="form-control" type="text">
                                    <span class="help-block"></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-8">Status</label>
                                <div class="col-md-9">
                                    <select disabled class="form-control" name="status" id="status_detail">
                                        <option value="true">Aktif</option>
                                        <option value="false">Tidak Aktif</option>
                                    </select>
                                    <span class="help-block"></span>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Tutup</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

    <script type="text/javascript">
        var save_method;

        function tambah_karyawan() {
            save_method = 'add';
            $('#form')[0].reset(); // reset form on modals
            $('.form-group').removeClass('has-error'); // clear error class
            $('.help-block').empty(); // clear error string
            $('#modal_form').modal({
                backdrop: 'static',
                keyboard: false
            }); // show bootstrap modal
            $('.modal-title').text('Tambah Data karyawan');

            // $('.modal-title').text('Tambah Data karyawan'); // Set Title to Bootstrap modal title
            $('#but_read').click(function() {
                var username = $('#selUser option:selected').text();
                var userid = $('#selUser').val();

                $('#result').html("id : " + userid + ", name : " + username);

            });
        }

        function detail_karyawan(nip) {
            $.ajax({
                type: "POST",
                url: "<?php echo base_url('karyawan/edit_karyawan') ?>",
                data: ({
                    nip: nip
                }),
                dataType: "json",
                success: function(response) {
                    $('#nip_detail').val(response.nip);
                    $('#ket_detail').val(response.ket);
                    $('#jenis_kelamin_detail').val(response.jenis_kelamin);
                    $('#alamat_detail').val(response.alamat);
                    $('#no_telp_detail').val(response.no_telp);
                    $('#status_detail').val(response.status);
                    $('#modal_detail').modal('show');
                }
            });
        }

        function save() {
            nip = $('#nip').val();
            ket = $('#ket').val();
            jenis_kelamin = $('#jenis_kelamin').val();
            alamat = $('#alamat').val();
            no_telp = $('#no_telp').val();
            status = $('#status').val();

            $('#btnSave').text('saving...'); //change button text
            $('#btnSave').attr('disabled', true); //set button disable 
            var url;

            if (save_method == 'add') {
                url = "<?php echo site_url('karyawan/tambah_karyawan') ?>";
            } else {
                url = "<?php echo site_url('karyawan/update_karyawan') ?>";
            }

            // ajax adding data to database
            $.ajax({
                type: "post",
                url: url,
                data: ({
                    nip: nip,
                    ket: ket,
                    jenis_kelamin: jenis_kelamin,
                    alamat: alamat,
                    no_telp: no_telp,
                    status: status
                }),
                dataType: "json",
                success: function(response) {
                    console.log(response);
                    if (response.status == true) {
                        swal("Berhasil menyimpan data " + ket, {
                            icon: "success",
                        }).then((ok) => {
                            window.location.reload();
                        })
                        $('#btnSave').text('Simpan'); //change button text
                        $('#btnSave').attr('disabled', false); //set button disable 
                        $('#modal_form').modal('hide');
                    } else {
                        swal(response.msg, {
                            icon: "warning",
                        }).then((ok) => {
                            $('#btnSave').text('Simpan'); //change button text
                            $('#btnSave').attr('disabled', false); //set button disable 
                        })
                    }

                }
            });
        }

        function edit_karyawan(nip) {
            $.ajax({
                type: "POST",
                url: "<?php echo base_url('karyawan/detail_karyawan') ?>",
                data: ({
                    nip: nip
                }),
                dataType: "json",
                success: function(response) {
                    $('#nip').val(response.nip);
                    $('#ket').val(response.ket);
                    $('#jenis_kelamin').val(response.jenis_kelamin);
                    $('#alamat').val(response.alamat);
                    $('#no_telp').val(response.no_telp);
                    $('#status').val(response.status);
                    $('#modal_form').modal('show');
                }
            });
        }

        function delete_karyawan(nip) {
            swal({
                    title: "Hapus Data Karyawan",
                    text: "Data karyawan akan hilang dalam database!",
                    icon: "warning",
                    buttons: true,
                    dangerMode: true,
                })
                .then((willDelete) => {
                    if (willDelete) {
                        $.ajax({
                            type: "post",
                            url: "<?php echo base_url('karyawan/delete_karyawan') ?>",
                            data: ({
                                nip: nip
                            }),
                            dataType: "json",
                            success: function(response) {
                                swal("Data Karyawan" + nip + "berhasil dihapus", {
                                    icon: "success",
                                }).then((ok) => {
                                    window.location.reload();
                                });
                            }
                        });
                    } else {
                        swal("Data Karyawan Aman");
                    }
                });
        }
    </script>