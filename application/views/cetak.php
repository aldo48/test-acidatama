<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Laporan</title>
    <style>
        .line-title {
            border: 0;
            border-style: inset;
            border-top: 1px solid #000;
        }
    </style>
</head>

<body>
    <table border="1" style="width: 100%;">
        <tr>
            <td align="center">
                <span style="line-height: 1.6; font-weight: bold;">
                    PT. INDO ACIDATAMA Tbk.
                    <br>KARANGANYAR INDONESIA
                </span>
            </td>
        </tr>
    </table>

    <hr class="line-title">
    <p align="center">
        DATA KARYAWAN<br>
    </p>
    <table border="1" class="table table-bordered">
        <tr>
            <th>No</th>
            <th>NIP</th>
            <th>Nama</th>
            <th>jenis_kelamin</th>
            <th>alamat</th>
            <th>no_telp</th>
            <th>status</th>
        </tr>
        <?php $no = 1;
        foreach ($data as $row) : ?>
            <tr>
                <td><?php echo $no++; ?></td>
                <td><?php echo $row['nip'] ?></td>
                <td><?php echo $row['ket'] ?></td>
                <td><?php echo $row['jenis_kelamin'] ?></td>
                <td><?php echo $row['alamat'] ?></td>
                <td><?php echo $row['no_telp'] ?></td>
                <td><?php echo ($row['status'] == "true") ? "Aktif" : "Tidak Aktif" ?></td>
            </tr>
        <?php endforeach ?>
    </table>
</body>

</html>